import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

public class SecondProject {

    public Logger LOGGER = LoggerFactory.getLogger(SecondProject.class);
    @Test
    public void LogExample() {
        LOGGER.info("Это сообщение будет выведено в лог");
        LOGGER.error("Определенно это ошибка");
    }
    @Test
    void checkCat() {
        int id = 36; // id number for tracking pet
        JSONObject cat = new JSONObject();
        cat.put("id", id);
        cat.put("name", "Буся");
        cat.put("status", "available");

        JSONObject updateCat = new JSONObject();
        updateCat.put("id", id);
        updateCat.put("name", "Барсик");
        updateCat.put("status", "available");

        given()
                .when()
                .contentType(ContentType.JSON)
                .body(cat.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all()
                .statusCode(200)
                .body(containsString("Буся"));

        given()
                .when()
                .contentType(ContentType.JSON)
                .body(updateCat.toString())
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all()
                .statusCode(200)
                .body(containsString("Барсик"));

        given()
                .when()
                .pathParam("petId", id)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(200)
                .body(containsString("Барсик"));

        given()
                .when()
                .log().all()
                .pathParam("petId", id)
                .delete("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(200);

        given()
                .when()
                .pathParam("petId", id)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(404)
                .body(containsString("Pet not found"));

    }

}
